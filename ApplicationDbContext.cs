using System.Diagnostics.CodeAnalysis;
using Microsoft.EntityFrameworkCore;
using vehicleservicemanagementv2.Models.Entities;

namespace vehicleservicemanagementv2
{
    public class ApplicationDbContext : DbContext
    {
        
        public ApplicationDbContext([NotNullAttribute] DbContextOptions options) : base(options)
        {
        }
        public DbSet<Vehicle> Vehicle {get;set;}
        public DbSet<Customer> Customer {get;set;}
        public DbSet<VehicleType> VehicleType {get;set;}
        public DbSet<Status> Status {get;set;}
        public DbSet<Service> Service {get;set;}
        public DbSet<VehicleToService> VehicleToService {get;set;}
    }
}