using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using vehicleservicemanagementv2.Models.Entities;
using vehicleservicemanagementv2.Models.Helpers;
using vehicleservicemanagementv2.Models.ViewModel;
using vehicleservicemanagementv2;

namespace vehicleservicemanagementv2.Controllers
{
    [Route("/Customer")]
    public class CustomerController : Controller
    {
        private readonly ApplicationDbContext dbContext;

        private CustomerOperations customerOperations;
        public CustomerController(ApplicationDbContext dbContext)
        {
            this.dbContext = dbContext;
            this.customerOperations = new CustomerOperations(this.dbContext);
        }

        [HttpGet("Get")]
        public async Task<ActionResult> Get()
        {
            var customers = await customerOperations.GetCustomer();
            return View(customers);
        }

        [HttpGet("Search")]
        public ActionResult Search()
        {
            return View();
        }

        [HttpPost("Search")]
        public async Task<ActionResult> Search(SearchModel searchModel)
        {
            if(!ModelState.IsValid)
            {
                return View();
            }
            if(searchModel.SearchParameter == "Name")
            {
                Regex exp = new Regex(@"^(([A-z]+[\s]{1}[A-z]+[\s]{1}[A-z]+)|([A-z]+[\s]{1}[A-z]+)|([A-z]+))$");
                if(!exp.IsMatch(searchModel.SearchValue))
                {
                    ViewBag.ValidationError = "Entered name is not in valid format";
                    return View();
                } 
            }
            else if(searchModel.SearchParameter == "Phone")
            {
                Regex exp = new Regex(@"[6-9]{1}[0-9]{9}");
                if(!exp.IsMatch(searchModel.SearchValue) || searchModel.SearchValue.Length != 10)
                {
                    ViewBag.ValidationError = "Phone number can start from [6 to 9 ] followed by nine other numbers";
                    return View();
                } 
            }
            else if(searchModel.SearchParameter == "Email")
            {
                Regex exp = new Regex(@"^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$");
                if(!exp.IsMatch(searchModel.SearchValue))
                {
                    ViewBag.ValidationError = "Please enter a valid Email address";
                    return View();
                } 
            }
            List<Customer> searchResult = await customerOperations.SearchCustomer(searchModel);
            if(searchResult == null)
            {
                ViewBag.NoSearchResult = "No Search Results found";
            }
            return View("Get",searchResult);
        }

        [HttpPost("Add")]
        public async Task<ActionResult> Add(Customer newCustomer)
        {   

            if (!ModelState.IsValid)
            {
                return View();
            }
            if(await customerOperations.AddCustomer(newCustomer))
            {
                return RedirectToAction("Get");
            } 
            else
            {
                ViewBag.AlreadyExists = "Entered phone number is already registered!!";
                return View();
            }    
        }
        [HttpGet("Add")]
        public ActionResult Add()
        {
            return View();    
        }

        [HttpGet("Delete/{Phone}")]
        public async Task<ActionResult> Delete([FromRoute] string Phone)
        {
            bool success = await customerOperations.DeleteCustomer(Phone);
            if(success)
            {
                return RedirectToAction("Get");
            }
            else
            {
                return RedirectToAction("Delete");
            }
        }

        [HttpGet("Update/{Id:int}")]
        public async Task<ActionResult> Update([FromRoute] int Id)
        {
            Customer customer = await dbContext.Customer.FirstOrDefaultAsync(x=> x.CustomerId == Id);
            return View(customer);
        }

        [HttpPost("Update/{Id:int}")]
        public async Task<ActionResult> Update(Customer updatedCustomer, [FromRoute] int Id)
        {
            if(!ModelState.IsValid)
            {
                return View();
            }
            bool isUpdated = await customerOperations.UpdateCustomer(updatedCustomer , Id);
            if(isUpdated)
            {
                return RedirectToAction("Get");
            }
            else
            {
                ViewBag.AlreadyExists = "Entered phone number is already registered!!";
                return View();
            }
        }
    } 
}