using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using vehicleservicemanagementv2.Models.Entities;
using vehicleservicemanagementv2.Models.Helpers;

namespace vehicleservicemanagementv2.Controllers
{
    [Route("/Vehicle")]
    public class VehicleController : Controller
    {
        private readonly ApplicationDbContext dbContext;
        private VehicleOperations vehicleOperations;
        public VehicleController(ApplicationDbContext dbContext)
        {
            this.dbContext = dbContext;
            this.vehicleOperations = new VehicleOperations(this.dbContext);
        }
        [HttpGet("{CustomerId:int}")]
        public async Task<ActionResult> GetByCustomerId ([FromRoute] int CustomerId)
        {
            Customer customer = await dbContext.Customer.FirstOrDefaultAsync(x=>x.CustomerId == CustomerId);
            ViewBag.Customer = customer;
            List<Vehicle> vehicles = vehicleOperations.GetByCustomerId(CustomerId);
            await GetVehicleType(vehicles);
            return View(vehicles);
        }

        [HttpPost("Add/{CustomerId:int}")]
        public async Task<ActionResult> Add(Vehicle newVehicle , [FromRoute] int CustomerId)
        {   
            newVehicle.CustomerId = CustomerId;
            newVehicle.RcNo =  newVehicle.RcNo.ToUpper();
            if (!ModelState.IsValid)
            {
                ViewBag.CustomerId = CustomerId;
                await BindVehicleTypeDropdownValues();
                return View();
            }
            if(await vehicleOperations.AddVehicle(newVehicle))
            {
                await GetVehicleType(newVehicle.VehicleTypeId);
                return RedirectToAction("GetByCustomerId" , new {newVehicle.CustomerId});
            } 
            else
            {
                ViewBag.CustomerId = CustomerId;
                await BindVehicleTypeDropdownValues();
                ViewBag.AlreadyExists = "Entered RC number is already registered!!";
                return View();
            }    
        }
        [HttpGet("Add/{CustomerId:int}")]
        public async Task<ActionResult> Add([FromRoute] int CustomerId)
        {
            ViewBag.CustomerId = CustomerId;
            await BindVehicleTypeDropdownValues();
            return View();
        }

        private async Task BindVehicleTypeDropdownValues()
        {
            var Types = await dbContext.VehicleType.ToListAsync();
            List<SelectListItem> vehicleTypes = new List<SelectListItem>();
            foreach (var type in Types)
            {
                vehicleTypes.Add(new SelectListItem { Value = type.VehicleTypeId, Text = type.Type });
            }
            ViewBag.VehicleTypes = vehicleTypes;
        }

        [HttpGet("Delete/{CustomerId:int}/{RcNo}")]
        public async Task<ActionResult> Delete( [FromRoute] int CustomerId, [FromRoute] string RcNo)
        {
            bool success = await vehicleOperations.DeleteVehicle(RcNo);
            if(success)
            {
                return RedirectToAction("GetByCUstomerId" , new{CustomerId});
            }
            else
            {
                return RedirectToAction("Delete");
            }
        }

        [HttpGet("Update/{RcNo}/{CustomerId:int}")]
        public async Task<ActionResult> Update([FromRoute] string RcNo)
        {
            Vehicle vehicle = await dbContext.Vehicle.FirstOrDefaultAsync(x=> x.RcNo == RcNo);
            await BindVehicleTypeDropdownValues();
            return View(vehicle);
        }

        [HttpPost("Update/{RcNo}/{CustomerId:int}")]
        public async Task<ActionResult> Update(Vehicle updatedVehicle, [FromRoute] string RcNo , [FromRoute] int CustomerId )
        {
            updatedVehicle.RcNo = updatedVehicle.RcNo.ToUpper();
            if(!ModelState.IsValid)
            {
                await BindVehicleTypeDropdownValues();
                return View();
            }
            bool isUpdated = await vehicleOperations.UpdateVehicle(updatedVehicle , RcNo);
            if(isUpdated)
            {
                await GetVehicleType(updatedVehicle.VehicleTypeId);
                return RedirectToAction("GetByCustomerId",new{CustomerId});
            }
            else
            {
                ViewBag.AlreadyExists = "Entered RC number is already registered!!";
                await BindVehicleTypeDropdownValues();
                return View();
            }
        }

        private async Task GetVehicleType(string VehicleTypeId)
        {
            var vehicleType = await dbContext.VehicleType.FirstOrDefaultAsync(x=> x.VehicleTypeId == VehicleTypeId);
            List<string> VehicleType = new List<string>();
            VehicleType.Add(vehicleType.Type); 
            ViewBag.VehicleType = VehicleType;
        }
        private async Task GetVehicleType(List<Vehicle> vehicles)
        {
            List<string> VehicleType = new List<string>();
            foreach(var vehicle in vehicles)
            {
                var vehicleType = await dbContext.VehicleType.FirstOrDefaultAsync(x=> x.VehicleTypeId == vehicle.VehicleTypeId);
                VehicleType.Add(vehicleType.Type);
            }
            ViewBag.VehicleType = VehicleType;    
        }
    }
}