using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using vehicleservicemanagementv2.Models.Entities;
using vehicleservicemanagementv2.Models.Helpers;
using vehicleservicemanagementv2.Models.ViewModel;
using RouteAttribute = Microsoft.AspNetCore.Components.RouteAttribute;

namespace vehicleservicemanagementv2.Controllers
{
    public class VehicleToServiceController:Controller
    {
        private readonly ApplicationDbContext dbContext;
        private ServiceOperations serviceOperations;
        public VehicleToServiceController(ApplicationDbContext dbContext)
        {
            this.dbContext = dbContext;
            this.serviceOperations = new ServiceOperations(this.dbContext);
        }
        
        [HttpGet("Service/{VehicleTypeId}/{RcNo}")]
        public ActionResult AddService([FromRoute] string VehicleTypeId , [FromRoute] string RcNo)
        {
            List<AddServiceModel> AvailableServices = serviceOperations.GetAvailableServices(VehicleTypeId);
            ViewBag.VehicleTypeId = VehicleTypeId;
            ViewBag.RcNo = RcNo;
            return View(AvailableServices);
        }

        [HttpPost("Service/{VehicleTypeId}/{RcNo}")]
        public async Task<ActionResult> AddServiceAsync (List<AddServiceModel> SelectedServiceIds , [FromRoute] string RcNo)
        {
            if(SelectedServiceIds.Count== 0)
            {
                return View();
            }
            await serviceOperations.GetSelectedService(SelectedServiceIds , RcNo); 
            return RedirectToAction("Cart" , new{RcNo});   
        }

        [HttpGet("Service/Cart/{RcNo}")]
        public async Task<ActionResult> Cart( [FromRoute] string RcNo)
        {
            List<Service> InCartServices = new List<Service>();
            InCartServices = await serviceOperations.GetCart(RcNo);
            ViewBag.RcNo = RcNo;
            return View(InCartServices);
        }

        [HttpPost("Service/Cart/{RcNo}")]
        public async Task<ActionResult> Cart(List<Service> InCartServices , [FromRoute] string RcNo)
        {
            
            ViewBag.Estimations = await serviceOperations.PlaceOrder(InCartServices,RcNo);
            Vehicle vehicle = await dbContext.Vehicle.FirstOrDefaultAsync(x=>x.RcNo == RcNo);
            Customer customer = await dbContext.Customer.FirstOrDefaultAsync(x=>x.CustomerId == vehicle.CustomerId);
            ViewBag.Customer = customer;
            ViewBag.Vehicle = vehicle;
            var VehicleType = await dbContext.VehicleType.FirstOrDefaultAsync(x=> x.VehicleTypeId == vehicle.VehicleTypeId);
            ViewBag.VehicleType = VehicleType.Type;
            
            return View("Order" , InCartServices);
        }

        [HttpGet("Service/Delete/{RcNo}/{ServiceId}")]
        public async Task<ActionResult> DeleteFromCart([FromRoute] string ServiceId , [FromRoute] string RcNo)
        {
            await serviceOperations.RemoveService(ServiceId , RcNo);
            ViewBag.RcNo = RcNo;
            return RedirectToAction("Cart" , new{RcNo});
        }
    }
}

