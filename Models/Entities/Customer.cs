using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace vehicleservicemanagementv2.Models.Entities
{
    public class Customer
    {
        [Key]
        public int CustomerId { get; set; } 

        [Required]
        [MaxLength(10)]
        [RegularExpression(@"[6-9]{1}[0-9]{9}",ErrorMessage ="Phone number can start from [6 to 9 ] followed by nine other numbers")]
        public string Phone { get; set; }
        
        [Required]
        [MaxLength(40)]
        [RegularExpression(@"^(([A-z]+[\s]{1}[A-z]+[\s]{1}[A-z]+)|([A-z]+[\s]{1}[A-z]+)|([A-z]+))$",ErrorMessage ="Name can have alphabets only [A to Z] or [a to z]")]
        public string Name { get; set; }

        [Required]
        [MaxLength(200)]
        public string Address { get; set; }

        [Required]
        [EmailAddress]
        [MaxLength(50)]
        public string Email { get; set; }
    }
}

/*
<div style="padding-right: 2%; float:right;">
    <a href="/Home/Index" class="btn btn-primary">Return</a>
</div>
<div class="row text-center" style="padding: 5%;">
    <div class="col-lg-6 col-md-6 col-sm-12">
        @using(Html.BeginForm()){
        @Html.ValidationSummary(true)
        <fieldset style="padding-left: 5%;">
            <legend>Search By Phone </legend>

            <div class="editor-label">
                @Html.LabelFor(model => model.Phone)
            </div>
            <div class="editor-field">
                @Html.EditorFor(model => model.Phone)
                @Html.ValidationMessageFor(model => model.Phone)
            </div>

            <br>
            <input type="submit" value="Search" class="btn btn-success btn-lg">
            <br>
        </fieldset>
        }
    </div>
    <div class="col-lg-6 col-md-6 col-sm-12">
        @using(Html.BeginForm()){
        @Html.ValidationSummary(true)
        <fieldset style="padding-left: 5%;">
            <legend>Search By Name </legend>

            <div class="editor-label">
                @Html.LabelFor(model => model.Name)
            </div>
            <div class="editor-field">
                @Html.EditorFor(model => model.Name)
                @Html.ValidationMessageFor(model => model.Name)
            </div>
            <br>
            <input type="submit" value="Search" class="btn btn-success btn-lg">
            <br>
            
        </fieldset>
        }
    </div>
</div>
s
*/