using System;
using System.ComponentModel.DataAnnotations;

namespace vehicleservicemanagementv2.Models.Entities
{
    public class Service
    {
        [Key]
        public string ServiceId{ get; set; }
        public string VehicleTypeId { get; set; }
        public string Name { get; set; }
        public int Cost { get; set; }
        public TimeSpan Time { get; set;}
    }
}