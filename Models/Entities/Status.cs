using System.ComponentModel.DataAnnotations;

namespace vehicleservicemanagementv2.Models.Entities
{
    public class Status
    {
        [Key]
        public string StatusId { get; set; }
        [Required]
        public string StatusCode { get; set; }
    }
}