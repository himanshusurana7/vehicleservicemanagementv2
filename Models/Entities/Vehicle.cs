using System.ComponentModel.DataAnnotations;

namespace vehicleservicemanagementv2.Models.Entities
{
    public class Vehicle
    {
        [Key]
        public int VehicleId {get;set;}
        [MaxLength(10)]
        [Required]
        [RegularExpression(@"^[A-Za-z0-9_-]*$", ErrorMessage ="only Alphabets and numbers are allowed.")]
        public string RcNo {get;set;}
        [Required]
        public int  CustomerId {get;set;}
        [Required]
        public string VehicleTypeId {get;set;}
        [Required]
        public string Model{get;set;}
    }
}