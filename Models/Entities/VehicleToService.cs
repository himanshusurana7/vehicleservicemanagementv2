using System;
using System.ComponentModel.DataAnnotations;

namespace vehicleservicemanagementv2.Models.Entities
{
    public class VehicleToService
    {
        [Key]
        public int VehicleToServiceId { get; set; }
        [Required]
        public string ServiceId { get; set; }
        [Required]
        public string RcNo { get; set; }
        [Required]
        public DateTime StartedAt { get ; set; }
        public DateTime CompletedAt { get ; set; }
        public string StatusId { get; set; }
    }   
}