using System.ComponentModel.DataAnnotations;

namespace vehicleservicemanagementv2.Models.Entities
{
    public class VehicleType
    {
        [Key]
        public string VehicleTypeId{get;set;}
        public string Type {get;set;} 
    }
}

