using System.Collections.Generic;
using vehicleservicemanagementv2.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using System;
using Microsoft.AspNetCore.Mvc;
using System.Reflection;
using System.Linq;
using vehicleservicemanagementv2.Models.ViewModel;
using vehicleservicemanagementv2;

namespace vehicleservicemanagementv2.Models.Helpers
{
    public class CustomerOperations
    {
        private readonly ApplicationDbContext dbContext;

        public CustomerOperations(ApplicationDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public async Task<List<Customer>> GetCustomer()
        {
            var customers = await dbContext.Customer.ToListAsync();
            return customers;
        }

        public async Task<bool> AddCustomer(Customer newCustomer)
        {
            Customer searchResult = await dbContext.Customer.FirstOrDefaultAsync(x=> x.Phone == newCustomer.Phone );
            if(searchResult != null)
            {
                return false;
            }
            newCustomer.Name.Trim();
            await dbContext.Customer.AddAsync(newCustomer);
            await dbContext.SaveChangesAsync();
            return true;
        }

        public async Task<List<Customer>> SearchCustomer(SearchModel searchModel)
        {
            PropertyInfo SearchParameter = null;
            foreach(var property in typeof(Customer).GetProperties())
            {
                if(property.Name == searchModel.SearchParameter)
                {
                    SearchParameter = property;
                }
            }
            List<Customer> allCustomer = await GetCustomer();
            var searchResult = from customer in allCustomer where SearchParameter.GetValue(customer).ToString() == searchModel.SearchValue select(customer);
            if(searchResult == null)
            {
                return null;
            }
            List<Customer> customers = new List<Customer>();
            foreach(var customer in searchResult)
            {
                customers.Add(customer);
            }
            return customers; 
        }

        public async Task<bool> DeleteCustomer(string Phone)
        {
            Customer searchResult = await dbContext.Customer.FirstOrDefaultAsync(x => x.Phone == Phone);
            if(searchResult == null)
            {
                return false;
            }
            else
            {
                VehicleOperations vehicleOperations = new VehicleOperations(dbContext);
                List<Vehicle> VehiclesOfCustomer = vehicleOperations.GetByCustomerId(searchResult.CustomerId);
                if(VehiclesOfCustomer != null)
                {
                    foreach(var vehicle in VehiclesOfCustomer)
                    {
                        await vehicleOperations.DeleteVehicle(vehicle.RcNo);
                    }
                }
                dbContext.Customer.Remove(searchResult);
                await dbContext.SaveChangesAsync();
                return true;
            }
        }

        public async Task<bool> UpdateCustomer(Customer updatedCustomer , int Id)
        {
            Customer oldCustomer = await dbContext.Customer.FirstOrDefaultAsync(x => x.CustomerId == Id );
            Customer newCustomer = await dbContext.Customer.FirstOrDefaultAsync(x => x.Phone == updatedCustomer.Phone);
            if(newCustomer == null || newCustomer.CustomerId == oldCustomer.CustomerId)
            {
                oldCustomer.Name = updatedCustomer.Name.Trim();
                oldCustomer.Phone = updatedCustomer.Phone;
                oldCustomer.Email = updatedCustomer.Email;
                oldCustomer.Address = updatedCustomer.Address;
                await dbContext.SaveChangesAsync();
                return true;
            }
            else
            {
                return false;
            }
        }
    } 
}