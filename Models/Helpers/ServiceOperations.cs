using System.Collections.Generic;
using System.Threading.Tasks;
using vehicleservicemanagementv2.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using vehicleservicemanagementv2.Models.ViewModel;
using System;

namespace vehicleservicemanagementv2.Models.Helpers
{
    public class ServiceOperations
    {
        private readonly ApplicationDbContext dbContext;

        public ServiceOperations(ApplicationDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public  List<AddServiceModel> GetAvailableServices(string VehicleTypeId)
        {
            var services = from service in dbContext.Service where service.VehicleTypeId == VehicleTypeId select(service);
            List<AddServiceModel> availableServices = new List<AddServiceModel>();
            foreach(var service in services)
            {
                availableServices.Add(new AddServiceModel{service = service,isSelected = false});
            }
            return availableServices;
        } 

        public async Task GetSelectedService (List<AddServiceModel> selectedServiceModels , string RcNo)
        {
            List<Service> selectedServices = new List<Service>();
            foreach(AddServiceModel serviceModel in selectedServiceModels)
            {
                if(serviceModel.isSelected)
                {
                    selectedServices.Add(await dbContext.Service.FirstOrDefaultAsync(x => x.ServiceId == serviceModel.service.ServiceId));
                }
            }
            foreach(Service service in selectedServices)
            {
                {
                    var searchResult = await dbContext.VehicleToService.FirstOrDefaultAsync(x=> x.RcNo == RcNo && x.ServiceId ==service.ServiceId && x.StatusId != "4");
                    if(searchResult != null)
                    {
                        continue;
                    }
                    else
                    {
                        await dbContext.VehicleToService.AddAsync(new VehicleToService {
                        ServiceId = service.ServiceId,
                        RcNo = RcNo,
                        StartedAt= DateTime.MinValue,
                        CompletedAt= DateTime.MinValue,
                        StatusId = "1" 
                    });
                    await dbContext.SaveChangesAsync();
                    }
                
                }
                
            }
        }
        public async Task<List<Service>> GetCart(string RcNo)
        {
            var alltasks = await dbContext.VehicleToService.ToListAsync();
            var activetasks = from activetask in alltasks where activetask.RcNo.Equals(RcNo) && activetask.StatusId.Equals("1") select(activetask);
            List<Service> inCartServices = new List<Service>();
            foreach(var task in activetasks)
            {
                Service service = await dbContext.Service.FirstOrDefaultAsync(x=> x.ServiceId == task.ServiceId);
                inCartServices.Add(service);
            }
            
            return inCartServices;
        }
        public async Task RemoveService(string ServiceId , string RcNo)
        {   
            VehicleToService ToRemoveService = await dbContext.VehicleToService.FirstOrDefaultAsync(x=> x.StatusId.Equals("1") && x.RcNo == RcNo && x.ServiceId == ServiceId);
            dbContext.VehicleToService.Remove(ToRemoveService);
            await dbContext.SaveChangesAsync();        
        }

        public async Task<TimeAndCost> PlaceOrder(List<Service> finalServices , string RcNo)
        {
            var alltasks = await dbContext.VehicleToService.ToListAsync();
            var activetasks = from activetask in alltasks where activetask.RcNo.Equals(RcNo) && activetask.StatusId.Equals("1") select(activetask);
            int totalCost = 0;
            DateTime estimatedTime = DateTime.Now;
            foreach(var task in activetasks)
            {
                task.StatusId = "2";
            } 

            foreach(var services in finalServices)
            {
                
                totalCost += services.Cost;
                estimatedTime = estimatedTime.Add(services.Time);
            }
            await dbContext.SaveChangesAsync();
            return (new TimeAndCost{
                Cost = totalCost,
                Time = estimatedTime
            });
        }
    }
}