using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using vehicleservicemanagementv2.Models.Entities;

namespace vehicleservicemanagementv2.Models.Helpers
{
    public class VehicleOperations
    {
        private readonly ApplicationDbContext dbContext;

        public VehicleOperations(ApplicationDbContext dbContext)
        {
            this.dbContext = dbContext;
        }
        public List<Vehicle> GetByCustomerId(int CustomerId)
        {
            var vehicles = from vehicle in dbContext.Vehicle where vehicle.CustomerId == CustomerId select(vehicle);
            return vehicles.ToList();
        }
        public async Task<bool> AddVehicle(Vehicle newVehicle)
        {
            Vehicle searchResult = await dbContext.Vehicle.FirstOrDefaultAsync(x=> x.RcNo == newVehicle.RcNo );
            if(searchResult != null)
            {
                return false;
            }
            await dbContext.Vehicle.AddAsync(newVehicle);
            await dbContext.SaveChangesAsync();
            return true;
        }

        public async Task<bool> UpdateVehicle(Vehicle updatedVehicle , string RcNo)
        {
            Vehicle oldVehicle = await dbContext.Vehicle.FirstOrDefaultAsync(x => x.RcNo == RcNo );
            Vehicle newVehicle = await dbContext.Vehicle.FirstOrDefaultAsync(x => x.RcNo == updatedVehicle.RcNo);
            if(newVehicle == null || newVehicle.RcNo == oldVehicle.RcNo)
            {
                oldVehicle.RcNo = updatedVehicle.RcNo;
                oldVehicle.Model = updatedVehicle.Model;
                oldVehicle.VehicleTypeId = updatedVehicle.VehicleTypeId;
                await dbContext.SaveChangesAsync();
                return true;
            }
            else
            {
                return false;
            }
        }
        public async Task<bool> DeleteVehicle(string RcNo)
        {
            Vehicle searchResult = await dbContext.Vehicle.FirstOrDefaultAsync(x => x.RcNo.Equals(RcNo));
            if(searchResult == null)
            {
                return false;
            }
            else
            {
                dbContext.Vehicle.Remove(searchResult);
                await dbContext.SaveChangesAsync();
                return true;
            }                
        }
    }

    
}

