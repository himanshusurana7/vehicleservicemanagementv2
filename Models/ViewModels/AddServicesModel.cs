using System.Collections.Generic;
using vehicleservicemanagementv2.Models.Entities;

namespace vehicleservicemanagementv2.Models.ViewModel
{
    public class AddServiceModel
    {
        public Service service { get; set; }
        public bool isSelected { get; set; }
    }
}