using System.ComponentModel.DataAnnotations;

namespace vehicleservicemanagementv2.Models.ViewModel
{
    public class SearchModel
    {
        [Required]
        public string SearchParameter{get;set;}
        [Required]
        public string SearchValue{get;set;}
    }
}