using System.Collections.Generic;
using vehicleservicemanagementv2.Models.Entities;

namespace vehicleservicemanagementv2.Models.ViewModel
{
    public class ServiceList
    {
        public List<Service> services {get;set;}
    }
}