using System;

namespace vehicleservicemanagementv2.Models.ViewModel
{
    public class TimeAndCost
    {
        public int Cost {get;set;}
        public DateTime Time {get;set;}
    }
}