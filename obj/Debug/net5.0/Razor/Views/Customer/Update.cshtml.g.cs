#pragma checksum "C:\Users\himanshu.surana\source\repos\vehicleservicemanagementv2\Views\Customer\Update.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "9a23e10b3b21808aa5d8ae01fe7f1d438c0be939"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Customer_Update), @"mvc.1.0.view", @"/Views/Customer/Update.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\himanshu.surana\source\repos\vehicleservicemanagementv2\Views\_ViewImports.cshtml"
using vehicleservicemanagementv2;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\himanshu.surana\source\repos\vehicleservicemanagementv2\Views\_ViewImports.cshtml"
using vehicleservicemanagementv2.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"9a23e10b3b21808aa5d8ae01fe7f1d438c0be939", @"/Views/Customer/Update.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"3f9b77fc77fc16990bfd9acd2680a765b09b8b36", @"/Views/_ViewImports.cshtml")]
    public class Views_Customer_Update : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<vehicleservicemanagementv2.Models.Entities.Customer>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
#nullable restore
#line 3 "C:\Users\himanshu.surana\source\repos\vehicleservicemanagementv2\Views\Customer\Update.cshtml"
  
    ViewData["Title"] = "CustomerSearch";

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
    <style type=""text/css"">
        /*body{
            background-image: url(""/Images/AddCustomerBackground2.jpg"");
            background-repeat: no-repeat;
            background-size: cover; 
        }*/
    </style>
        <div class=""text-center"">
            <h1 class=""display-4"">Update Customer</h1>
        </div>
        <div style=""padding-left: 5%;"">
");
#nullable restore
#line 18 "C:\Users\himanshu.surana\source\repos\vehicleservicemanagementv2\Views\Customer\Update.cshtml"
             using(Html.BeginForm()){
            

#line default
#line hidden
#nullable disable
#nullable restore
#line 19 "C:\Users\himanshu.surana\source\repos\vehicleservicemanagementv2\Views\Customer\Update.cshtml"
       Write(Html.ValidationSummary(true));

#line default
#line hidden
#nullable disable
            WriteLiteral("            <fieldset>\r\n                <legend class=\"display-5\">Customer</legend>\r\n                \r\n                <div class=\"editor-label required\">\r\n                    ");
#nullable restore
#line 24 "C:\Users\himanshu.surana\source\repos\vehicleservicemanagementv2\Views\Customer\Update.cshtml"
               Write(Html.LabelFor(model => model.Name));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </div>\r\n                <div class=\"editor-field\">\r\n                    ");
#nullable restore
#line 27 "C:\Users\himanshu.surana\source\repos\vehicleservicemanagementv2\Views\Customer\Update.cshtml"
               Write(Html.TextBoxFor(model => model.Name , new{style = "width: 300px"}));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    ");
#nullable restore
#line 28 "C:\Users\himanshu.surana\source\repos\vehicleservicemanagementv2\Views\Customer\Update.cshtml"
               Write(Html.ValidationMessageFor(model => model.Name));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </div>\r\n\r\n                <div class=\"editor-label required\">\r\n                    ");
#nullable restore
#line 32 "C:\Users\himanshu.surana\source\repos\vehicleservicemanagementv2\Views\Customer\Update.cshtml"
               Write(Html.LabelFor(model => model.Email));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </div>\r\n                <div class=\"editor-field\">\r\n                    ");
#nullable restore
#line 35 "C:\Users\himanshu.surana\source\repos\vehicleservicemanagementv2\Views\Customer\Update.cshtml"
               Write(Html.TextAreaFor(model => model.Email , new{style = "width: 300px"}));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    ");
#nullable restore
#line 36 "C:\Users\himanshu.surana\source\repos\vehicleservicemanagementv2\Views\Customer\Update.cshtml"
               Write(Html.ValidationMessageFor(model => model.Email));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </div>\r\n\r\n                <div class=\"editor-label required\">\r\n                    ");
#nullable restore
#line 40 "C:\Users\himanshu.surana\source\repos\vehicleservicemanagementv2\Views\Customer\Update.cshtml"
               Write(Html.LabelFor(model => model.Phone));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </div>\r\n                <div class=\"editor-field\">\r\n                    ");
#nullable restore
#line 43 "C:\Users\himanshu.surana\source\repos\vehicleservicemanagementv2\Views\Customer\Update.cshtml"
               Write(Html.TextBoxFor(model => model.Phone , new{style = "width: 300px"}));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    ");
#nullable restore
#line 44 "C:\Users\himanshu.surana\source\repos\vehicleservicemanagementv2\Views\Customer\Update.cshtml"
               Write(Html.ValidationMessageFor(model => model.Phone));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </div>\r\n\r\n                <div class=\"editor-label required\">\r\n                    ");
#nullable restore
#line 48 "C:\Users\himanshu.surana\source\repos\vehicleservicemanagementv2\Views\Customer\Update.cshtml"
               Write(Html.LabelFor(model => model.Address));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </div>\r\n                <div class=\"editor-field\">\r\n                    ");
#nullable restore
#line 51 "C:\Users\himanshu.surana\source\repos\vehicleservicemanagementv2\Views\Customer\Update.cshtml"
               Write(Html.TextAreaFor(model => model.Address , new{style = "width: 300px"}));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    ");
#nullable restore
#line 52 "C:\Users\himanshu.surana\source\repos\vehicleservicemanagementv2\Views\Customer\Update.cshtml"
               Write(Html.ValidationMessageFor(model => model.Address));

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
                </div>
                <br>
                <span><a href=""/Customer/Get"" class=""btn btn-lg btn-outline-dark"" style=""width: 150px;"">Return</a> </span>
            <span><input type=""submit"" class=""btn btn-lg btn-outline-dark"" style=""width: 150px;"" value=""Update""> </span>
            </fieldset>
");
            WriteLiteral("        <br>\r\n        <h3>");
#nullable restore
#line 60 "C:\Users\himanshu.surana\source\repos\vehicleservicemanagementv2\Views\Customer\Update.cshtml"
       Write(ViewBag.AlreadyExists);

#line default
#line hidden
#nullable disable
            WriteLiteral("</h3>\r\n        <br>\r\n");
#nullable restore
#line 62 "C:\Users\himanshu.surana\source\repos\vehicleservicemanagementv2\Views\Customer\Update.cshtml"

        }

#line default
#line hidden
#nullable disable
            WriteLiteral("        </div>\r\n        \r\n    ");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<vehicleservicemanagementv2.Models.Entities.Customer> Html { get; private set; }
    }
}
#pragma warning restore 1591
